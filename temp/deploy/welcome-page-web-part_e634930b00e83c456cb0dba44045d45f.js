define("4837ff42-760c-4165-b558-0bdae9f5c425_0.0.1", ["@microsoft/sp-property-pane","@microsoft/sp-loader","@microsoft/sp-core-library","WelcomePageWebPartStrings","@microsoft/sp-webpart-base","react","react-dom"], function(__WEBPACK_EXTERNAL_MODULE__26ea__, __WEBPACK_EXTERNAL_MODULE_I6O9__, __WEBPACK_EXTERNAL_MODULE_UWqr__, __WEBPACK_EXTERNAL_MODULE_adv5__, __WEBPACK_EXTERNAL_MODULE_br4S__, __WEBPACK_EXTERNAL_MODULE_cDcd__, __WEBPACK_EXTERNAL_MODULE_faye__) { return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "HTjn");
/******/ })
/************************************************************************/
/******/ ({

/***/ "26ea":
/*!**********************************************!*\
  !*** external "@microsoft/sp-property-pane" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE__26ea__;

/***/ }),

/***/ "HTjn":
/*!********************************************************!*\
  !*** ./lib/webparts/welcomePage/WelcomePageWebPart.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "faye");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _microsoft_sp_core_library__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @microsoft/sp-core-library */ "UWqr");
/* harmony import */ var _microsoft_sp_core_library__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_microsoft_sp_core_library__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _microsoft_sp_property_pane__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @microsoft/sp-property-pane */ "26ea");
/* harmony import */ var _microsoft_sp_property_pane__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_microsoft_sp_property_pane__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _microsoft_sp_webpart_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @microsoft/sp-webpart-base */ "br4S");
/* harmony import */ var _microsoft_sp_webpart_base__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_microsoft_sp_webpart_base__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var WelcomePageWebPartStrings__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! WelcomePageWebPartStrings */ "adv5");
/* harmony import */ var WelcomePageWebPartStrings__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(WelcomePageWebPartStrings__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _components_WelcomePage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/WelcomePage */ "el8Y");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();







var renderContent;
// if((/welcome.aspx/.test(window.location.href.toLowerCase()) )){
//   console.log(window.location.href.toLowerCase());
//   renderContent = WelcomePage;
// }else if((/home.aspx/.test(window.location.href.toLowerCase()) )){
//   console.log(window.location.href.toLowerCase());
//   renderContent = HomePage;
// }else {
//   console.log(window.location.href.toLowerCase());
//   renderContent = HomePage;
// }
var WelcomePageWebPart = /** @class */ (function (_super) {
    __extends(WelcomePageWebPart, _super);
    function WelcomePageWebPart() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    WelcomePageWebPart.prototype.render = function () {
        var element = react__WEBPACK_IMPORTED_MODULE_0__["createElement"](_components_WelcomePage__WEBPACK_IMPORTED_MODULE_6__["default"], {
            description: this.properties.description
        });
        react_dom__WEBPACK_IMPORTED_MODULE_1__["render"](element, this.domElement);
    };
    WelcomePageWebPart.prototype.onDispose = function () {
        react_dom__WEBPACK_IMPORTED_MODULE_1__["unmountComponentAtNode"](this.domElement);
    };
    Object.defineProperty(WelcomePageWebPart.prototype, "dataVersion", {
        get: function () {
            return _microsoft_sp_core_library__WEBPACK_IMPORTED_MODULE_2__["Version"].parse('1.0');
        },
        enumerable: true,
        configurable: true
    });
    WelcomePageWebPart.prototype.getPropertyPaneConfiguration = function () {
        return {
            pages: [
                {
                    header: {
                        description: WelcomePageWebPartStrings__WEBPACK_IMPORTED_MODULE_5__["PropertyPaneDescription"]
                    },
                    groups: [
                        {
                            groupName: WelcomePageWebPartStrings__WEBPACK_IMPORTED_MODULE_5__["BasicGroupName"],
                            groupFields: [
                                Object(_microsoft_sp_property_pane__WEBPACK_IMPORTED_MODULE_3__["PropertyPaneTextField"])('description', {
                                    label: WelcomePageWebPartStrings__WEBPACK_IMPORTED_MODULE_5__["DescriptionFieldLabel"]
                                })
                            ]
                        }
                    ]
                }
            ]
        };
    };
    return WelcomePageWebPart;
}(_microsoft_sp_webpart_base__WEBPACK_IMPORTED_MODULE_4__["BaseClientSideWebPart"]));
/* harmony default export */ __webpack_exports__["default"] = (WelcomePageWebPart);


/***/ }),

/***/ "I6O9":
/*!***************************************!*\
  !*** external "@microsoft/sp-loader" ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_I6O9__;

/***/ }),

/***/ "UWqr":
/*!*********************************************!*\
  !*** external "@microsoft/sp-core-library" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_UWqr__;

/***/ }),

/***/ "adv5":
/*!********************************************!*\
  !*** external "WelcomePageWebPartStrings" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_adv5__;

/***/ }),

/***/ "br4S":
/*!*********************************************!*\
  !*** external "@microsoft/sp-webpart-base" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_br4S__;

/***/ }),

/***/ "cDcd":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_cDcd__;

/***/ }),

/***/ "el8Y":
/*!************************************************************!*\
  !*** ./lib/webparts/welcomePage/components/WelcomePage.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _microsoft_sp_loader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @microsoft/sp-loader */ "I6O9");
/* harmony import */ var _microsoft_sp_loader__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_microsoft_sp_loader__WEBPACK_IMPORTED_MODULE_1__);
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

//external imports

var _image = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module './Images/ManojNewImg.png'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
var WelcomePage = /** @class */ (function (_super) {
    __extends(WelcomePage, _super);
    function WelcomePage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // public componentDidMount(){
    //   $('div[name="Home"]').on('click', function(){
    //     window.location.reload();
    //   })
    // }
    WelcomePage.prototype.render = function () {
        _microsoft_sp_loader__WEBPACK_IMPORTED_MODULE_1__["SPComponentLoader"].loadCss("https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css");
        return (react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("div", null,
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("h5", null, "CEO's Message")),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("hr", null),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { style: { float: "left" }, className: "mr-3" },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("img", { src: _image, alt: "", style: { float: "left" } })),
            react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("span", { style: { textAlign: "justify" } },
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "YASH is on an exponential revenue growth path last several years.  We have become extremely intentional in our customer acquisition, given the focus on building long-term sustainable relationships. We want to help our customers realize business value from their technology investments and enable them to transform themselves in the process."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "We have been working to transform delivery globally and make it agile, focused on emerging best-practices, adopt global quality practices, drive profitability, and align it to deliver outstanding outcomes for our customers."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "Project and program management is a central theme in this delivery transformation. We are establishing a Global Project Management Office ( PMO), which will standardize best practices, guide projects to achieve success consistently, ensure strong governance, and develop a pervasive program management practice across the organization that drives delivery excellence."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "While  Derek Dyer is leading the Global PMO, Shivpal Singh and his team will run the India based central PMO.  They will interact and collaborate with Service lines, Sales, and Delivery teams in managing/helping manage critical projects."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null,
                    "Key focus areas of the Global PMO include",
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                    react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("ul", null,
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Consolidating PM Best practices within ( and from outside of) the organization and strengthen PM PMI methodologies within YASH."),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Outlining a consistent and well-structured methodology that governs all projects, safeguarding against project failures."),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Maintaining consistency between Projects  Managers by providing them access to templates, tools, repositories and other knowledge assets."),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Train-enable-evaluate-certify project and program managers in line with YASH requirements."),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Act as back up, join or lead large projects as necessary."),
                        react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("li", null, "Champion PM within YASH."))),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "Currently, many projects in YASH Americas and Europe of the SAP Serviceline are being governed-managed by the Global PMO. Starting August 2020, projects in these regions of the ILM and Digital service lines will come under the ambit of the PMO. Over time the idea is to bring the governance of all projects across regions under the stewardship of the Global PMO."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "In the long term, we will also establish a PM academy as part of the Global PMO, which will train-enable-evaluate-certify project managers and keep them abreast of the latest and greatest in the world of Program management."),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("br", null),
                react__WEBPACK_IMPORTED_MODULE_0__["createElement"]("p", null, "This is a strategic and business-critical initiative. I look forward to the leadership and other stakeholders working closely with the Global PMO in ensuring that every project that YASH delivers is exemplary and drives value for our clients."))));
    };
    return WelcomePage;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]));
/* harmony default export */ __webpack_exports__["default"] = (WelcomePage);


/***/ }),

/***/ "faye":
/*!****************************!*\
  !*** external "react-dom" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_faye__;

/***/ })

/******/ })});;
//# sourceMappingURL=welcome-page-web-part.js.map