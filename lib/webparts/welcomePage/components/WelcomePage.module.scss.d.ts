declare const styles: {
    welcomePage: string;
    container: string;
    row: string;
    column: string;
    'ms-Grid': string;
    title: string;
    subTitle: string;
    description: string;
    button: string;
    label: string;
};
export default styles;
//# sourceMappingURL=WelcomePage.module.scss.d.ts.map