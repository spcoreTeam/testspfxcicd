/* tslint:disable */
require("./WelcomePage.module.css");
var styles = {
    welcomePage: 'welcomePage_41181fa6',
    container: 'container_41181fa6',
    row: 'row_41181fa6',
    column: 'column_41181fa6',
    'ms-Grid': 'ms-Grid_41181fa6',
    title: 'title_41181fa6',
    subTitle: 'subTitle_41181fa6',
    description: 'description_41181fa6',
    button: 'button_41181fa6',
    label: 'label_41181fa6'
};
export default styles;
/* tslint:enable */ 
//# sourceMappingURL=WelcomePage.module.scss.js.map