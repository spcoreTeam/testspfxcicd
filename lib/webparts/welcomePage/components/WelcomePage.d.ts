import * as React from 'react';
import { IWelcomePageProps } from './IWelcomePageProps';
export default class WelcomePage extends React.Component<IWelcomePageProps, {}> {
    render(): React.ReactElement<IWelcomePageProps>;
}
//# sourceMappingURL=WelcomePage.d.ts.map