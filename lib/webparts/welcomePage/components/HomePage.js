var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
//external imports
import { SPComponentLoader } from "@microsoft/sp-loader";
var _image = require('./Images/Manoj1.png');
var HomePage = /** @class */ (function (_super) {
    __extends(HomePage, _super);
    function HomePage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HomePage.prototype.render = function () {
        SPComponentLoader.loadCss("https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css");
        return (React.createElement("div", { style: { fontFamily: "calibri", fontSize: "15px" } },
            React.createElement("div", null,
                React.createElement("h5", null, "CEO's Message")),
            React.createElement("hr", null),
            React.createElement("span", { style: { float: "left" }, className: "mr-3" },
                React.createElement("img", { src: _image, alt: "", style: { float: "left" } })),
            React.createElement("span", { style: { textAlign: "justify" } },
                React.createElement("p", null,
                    "YASH is on an exponential revenue growth path last several years.  We have become extremely intentional in our customer acquisition, given the focus on building long-term sustainable relationships. We want to help our customers realize business value from their technology investments and enable them to transform themselves in the process. \u00A0\u00A0\u00A0",
                    React.createElement("a", { href: "https://ytpl.sharepoint.com/sites/YASHPMO/SitePages/Welcome.aspx", target: "_blank", "data-interception": "off" }, "read more")))));
    };
    return HomePage;
}(React.Component));
export default HomePage;
//# sourceMappingURL=HomePage.js.map